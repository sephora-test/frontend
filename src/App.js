import React, { Component } from 'react';
import logo from './logo-company.png';
import './App.css';
import axios from 'axios';
import { Grid, Row, Col } from 'react-bootstrap';

class App extends Component {
  constructor(){
    super();
    this.state ={ contents: [] };
  }

  componentDidMount() {
    let url = 'https://sephora-test-api.herokuapp.com/api/contents?auth_token=b49475848ad661fa292184b3774fcabad70ed137d62d5c6fb972d628050879bd5620efd3143a26973c684e12fe96a4826737f53bee23f6c269c879f4423d5d52';
    axios.get(url).then(res => {
      console.log(res.data.data);
      this.setState({ contents: res.data.data });
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to GoldenOwl</h1>
        </header>
        <p className="App-intro">
        </p>
        <Grid>
          <Row className="row-header">
            <Col xs={6} md={2}>
              Title
            </Col>
            <Col xs={6} md={2}>
              Published Date
            </Col>
            <Col xs={6} md={2}>
              Author
            </Col>
            <Col xs={6} md={2}>
              Summary
            </Col>
            <Col xs={6} md={2}>
              Content
            </Col>
          </Row>
          {this.state.contents.map((value, i) => (
            <Row className="row-show-grid">
              <Col xs={6} md={2}>
                {value.title}
              </Col>
              <Col xs={6} md={2}>
                {value.published_date}
              </Col>
              <Col xs={6} md={2}>
                {value.author}
              </Col>
              <Col xs={6} md={2}>
                {value.summary}
              </Col>
              <Col xs={6} md={2}>
                {value.content}
              </Col>
            </Row>),
          )}
        </Grid>
      </div>
    );
  }
}

export default App;
